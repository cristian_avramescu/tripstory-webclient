import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import 'hammerjs';

// Application components
import { AppComponent } from './app.component';

// Application modules
import { CoreModule } from './core';
import { AppRouting } from './app.routing';
import { MainModule } from './main';
import { LoginModule } from './login';

const appModules = [
  AppRouting,
  CoreModule,
  MainModule,
  LoginModule
  ];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule.forRoot(),
    appModules
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
