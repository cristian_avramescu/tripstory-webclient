import { AbstractControl, ValidatorFn } from '@angular/forms';

export class GlobalValidators {
    static mail(control: AbstractControl) {
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if (control.value !== '' && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { 'incorrectEmailFormat': true };
        }

        return null;
    }

    static getEqualValidator(referenceControl: AbstractControl): ValidatorFn {
        return function (control: AbstractControl): { [key: string]: any } {
            let controlValue = control.value;
            let referenceValue = referenceControl.value;

            if (referenceValue &&
                controlValue &&
                controlValue !== referenceValue) {
                return {
                    validateEqual: true
                };
            }

            return null;
        };
    }
}
