import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login';
import { SignUpComponent } from './signup';
import { LoginGuard } from '../core';

const routes: Routes = [
    { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
    { path: 'login/new', component: SignUpComponent, canActivate: [LoginGuard] },
];

export const LoginRouting = RouterModule.forChild(routes);
