/* tslint:disable:no-unused-variable */
import { Component } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { SignUpComponent, IFormModel } from './signup.component';
import { CoreModule } from '../../core';
import { AccessTokenActions, UserActions } from '../../core';
import { RouterModuleTesting } from '../../core/test';
import { FormGroup } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'hammerjs';

describe('LoginModule', () => {
  describe('SignUpComponent', () => {
    let component: SignUpComponent;
    let fixture: ComponentFixture<SignUpComponent>;
    let formData: IFormModel = {
      name: 'test',
      email: 'test',
      password: 'pass',
      confirmPassword: 'pass'
    };

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [
          SignUpComponent
        ],
        providers: [
          AccessTokenActions,
          UserActions
        ],
        imports: [
          CoreModule,
          RouterModuleTesting
        ]
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SignUpComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create the component', () => {
      expect(component).toBeTruthy();
    });

    it('should create a form group', () => {
      expect(component.signUpForm instanceof FormGroup).toBeTruthy();
    });

    it('should create a form with the correct controls', () => {
      expect(Object.keys(component.signUpForm.controls)).toEqual([
        'name', 'email', 'password', 'confirmPassword'
      ]);
    });

    it('should add a required error on name if empty', () => {
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.signUpForm.controls['name'].errors).toEqual({ required: true });
    });

    it('should add a required error on email if empty', () => {
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.signUpForm.controls['email'].errors).toEqual({ required: true });
    });

    it('should add a required error on password if empty', () => {
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.signUpForm.controls['password'].errors).toEqual({ required: true });
    });

    it('should add a required error on confirmpassword if empty', () => {
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.signUpForm.controls['confirmPassword'].errors).toEqual({ required: true });
    });

    it('should add an email no valid error on email if wrong format', () => {
      let input: HTMLInputElement = fixture.nativeElement.querySelector('input[name=email]');
      component.signUpForm.controls['email'].setValue('toto');
      expect(component.signUpForm.controls['email'].errors).toEqual({ incorrectEmailFormat: true });
    });

    it('should add a passwords do not match if passwords different', () => {
      let input: HTMLInputElement = fixture.nativeElement.querySelector('input[name=email]');
      component.signUpForm.controls['password'].setValue('123');
      component.signUpForm.controls['confirmPassword'].setValue('1234');
      expect(component.signUpForm.controls['confirmPassword'].errors).toEqual({ validateEqual: true });
    });

    it('should route to home if sign up is correct', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      spyOn(component.usersService, 'signUp').and.returnValue(Observable.of(true));
      component.signUp(formData, true);
      expect(routerSpy).toHaveBeenCalledWith(['']);
    });

    it('should do nothing if form is invalid', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      let loginSpy = spyOn(component.usersService, 'signUp');
      component.signUp(formData, false);
      expect(routerSpy).not.toHaveBeenCalled();
      expect(loginSpy).not.toHaveBeenCalled();
    });

    it('should stay on the page if the response is not successfull', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      let loginSpy = spyOn(component.usersService, 'signUp').and.returnValue(Observable.of(false));

      component.signUp({
        name: 'test',
        email: 'test',
        password: 'not-pass',
        confirmPassword: 'not-pass'
      }, true);
      expect(routerSpy).not.toHaveBeenCalled();
    });

    it('should add uniqueness error on email if one already exists', () => {
      component.signUpForm.controls['email'].setValue('test@test.com');
      spyOn(component.usersService, 'signUp').and.returnValue(Observable.throw({
        error: {
          statusCode: 422,
          name: 'ValidationError',
          details: {
            codes: {
              email: ['uniqueness']
            }
          }
        }
      }));
      component.signUp(formData, true);
      expect(component.signUpForm.controls['email'].errors).toEqual({ uniqueness: true });
    });

    it('should add an error if password is invalid', () => {
      component.signUpForm.controls['password'].setValue('pass');
      spyOn(component.usersService, 'signUp').and.returnValue(Observable.throw({
        error: {
          statusCode: 422,
          name: 'Error',
          code: 'INVALID_PASSWORD'
        }
      }));
      component.signUp(formData, true);
      expect(component.signUpForm.controls['password'].errors).toEqual({ valid: true });
    });
  });
});
