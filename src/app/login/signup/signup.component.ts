import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { GlobalValidators } from '../validators';
import { UsersService } from '../../core';
import { ISignUpCredentials } from '../../models';
import { AutoUnsubscribe } from '../../decorators/autounsubscribe';

export interface IFormModel {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
}

@Component({
  styleUrls: ['../login.style.scss'],
  templateUrl: './signup.component.html'
})
@AutoUnsubscribe
export class SignUpComponent {
  signUpForm: FormGroup;
  submitted = false;

  userSignup$: Subscription = null;

  constructor(
    public usersService: UsersService,
    public router: Router,
    formBuilder: FormBuilder
  ) {
    this.signUpForm = formBuilder.group({
      name: ['', [<any>Validators.required]],
      email: ['', [<any>Validators.required, GlobalValidators.mail]],
      password: ['', [<any>Validators.required]],
      confirmPassword: [''],
    });
    let confirmPassword: AbstractControl = this.signUpForm.controls['confirmPassword'];
    let password: AbstractControl = this.signUpForm.controls['password'];
    confirmPassword.setValidators([Validators.required, GlobalValidators.getEqualValidator(password)]);
  }

  signUp(model: IFormModel, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      let newUser = {
        username: model.name,
        password: model.password,
        email: model.email
      };
      this.userSignup$ = this.usersService.signUp(newUser).subscribe(success => {
        if (success) {
          this.router.navigate(['']);
        }
      }, err => {
        let error = err.error;
        if (error.statusCode === 422) {
          if (error.name === 'ValidationError') {
            if (error.details.codes.email && error.details.codes.email[0] === 'uniqueness') {
              this.signUpForm.controls['email'].setErrors({
                uniqueness: true
              });
            }
          } else if (error.name === 'Error') {
            if (error.code === 'INVALID_PASSWORD') {
              this.signUpForm.controls['password'].setErrors({
                valid: true
              });
            }
          }
        }
        this.submitted = false;
      });
    }
  }

  isControlInError(control) {
    return !control.valid && (control.touched || this.submitted);
  }

}
