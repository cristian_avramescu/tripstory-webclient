import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { GlobalValidators } from '../validators';
import { UsersService } from '../../core';
import { ICredentials } from '../../models';
import { AutoUnsubscribe } from '../../decorators/autounsubscribe';

interface IFormGroup {
  email: string;
  password: string;
  keepLogin: boolean;
}

@Component({
  styleUrls: ['../login.style.scss'],
  templateUrl: './login.component.html'
})
@AutoUnsubscribe
export class LoginComponent {
  loginForm: FormGroup;
  loginFailed = false;
  submitted = false;

  userLogin$: Subscription = null;

  constructor(
    public usersService: UsersService,
    public router: Router,
    formBuilder: FormBuilder
  ) {
    this.loginForm = formBuilder.group({
      email: ['', [<any>Validators.required, GlobalValidators.mail]],
      password: ['', [<any>Validators.required]],
      keepLogin: [true]
    });
  }

  login(model: IFormGroup, isValid: boolean) {
    this.loginFailed = false;
    this.submitted = true;
    if (isValid) {
      let signInModel = {
        email: model.email,
        password: model.password
      };
      this.userLogin$ = this.usersService.login(model, model.keepLogin).subscribe((success) => {
        if (success) {
          this.router.navigate(['']);
        } else {
          this.loginFailed = true;
        }
        this.submitted = false;
      });
    }
  }

  isControlInError(control) {
    return !control.valid && (control.touched || this.submitted);
  }

}
