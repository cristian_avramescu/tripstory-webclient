/* tslint:disable:no-unused-variable */
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { CoreModule } from '../../core';
import { RouterModuleTesting } from '../../core/test';
import { FormGroup } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'hammerjs';

describe('LoginModule', () => {
  describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [
          LoginComponent
        ],
        providers: [],
        imports: [
          RouterModuleTesting,
          CoreModule
        ]
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create the component', () => {
      expect(component).toBeTruthy();
    });

    it('should create a form group', () => {
      expect(component.loginForm instanceof FormGroup).toBeTruthy();
    });

    it('should create a form with the correct controls', () => {
      expect(Object.keys(component.loginForm.controls)).toEqual([
        'email', 'password', 'keepLogin'
      ]);
    });

    it('should add a required error on email if empty', () => {
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.loginForm.controls['email'].errors).toEqual({ required: true });
    });

    it('should add an email no valid error on email if wrong format', () => {
      let input: HTMLInputElement = fixture.nativeElement.querySelector('input[name=email]');
      component.loginForm.controls['email'].setValue('toto');
      fixture.detectChanges();
      expect(component.loginForm.controls['email'].errors).toEqual({ incorrectEmailFormat: true });
    });

    it('should add a required error on password if empty', () => {
      fixture.detectChanges();
      let evt = document.createEvent('Event');
      evt.initEvent('input', true, false);
      dispatchEvent(evt);
      expect(component.loginForm.controls['password'].errors).toEqual({ required: true });
    });

    it('should route to home if login is correct', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      spyOn(component.usersService, 'login').and.returnValue(Observable.of(true));
      component.login({
        email: 'test',
        password: 'pass',
        keepLogin: false
      }, true);
      expect(routerSpy).toHaveBeenCalledWith(['']);
      expect(component.loginFailed).toBeFalsy();
    });

    it('should set loginfailed to true and do nothing if login is incorrect', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      spyOn(component.usersService, 'login').and.returnValue(Observable.of(false));
      component.login({
        email: 'test',
        password: 'not-pass',
        keepLogin: false
      }, true);
      expect(routerSpy).not.toHaveBeenCalled();
      expect(component.loginFailed).toBeTruthy();
    });

    it('should do nothing if form is invalid', () => {
      let routerSpy = spyOn(component.router, 'navigate');
      let loginSpy = spyOn(component.usersService, 'login');
      component.login({
        email: 'test',
        password: 'not-pass',
        keepLogin: false
      }, false);
      expect(routerSpy).not.toHaveBeenCalled();
      expect(loginSpy).not.toHaveBeenCalled();
    });
  });
});
