import { NgModule } from '@angular/core';
import { CoreModule } from '../core';
import { LoginRouting } from './login.routing';
import { LoginComponent } from './login';
import { SignUpComponent } from './signup';

@NgModule({
    imports: [CoreModule, LoginRouting],
    exports: [LoginComponent, SignUpComponent],
    declarations: [LoginComponent, SignUpComponent],
    providers: []
})
export class LoginModule { }
