import { TestBed, async, getTestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BaseRequestOptions, Http, XHRBackend, ResponseOptions, Response } from '@angular/http';
import { HttpService } from './http-service';
import { AuthServiceInternal } from '../auth-service';
import { AccessTokenActions, UserActions } from '../../../core';
import { HTTP_SERVICE_PROVIDERS, RouterModuleTesting, STORE_PROVIDERS } from '../../test';

describe('CoreModule', () => {
    describe('InternalServices', () => {
        describe('HttpService', () => {
            let backend: MockBackend;
            let service: HttpService;
            let routerService: Router;

            beforeEach(async(() => {
                TestBed.configureTestingModule({
                    providers: [
                        HTTP_SERVICE_PROVIDERS,
                        STORE_PROVIDERS,
                        HttpService,
                        AccessTokenActions,
                        UserActions,
                        AuthServiceInternal
                    ],
                    imports: [
                        RouterModuleTesting
                    ]
                });

                const testbed = getTestBed();
                backend = testbed.get(MockBackend);
                service = testbed.get(HttpService);
                routerService = testbed.get(Router);
                spyOn(routerService, 'navigate');
                spyOn(console, 'error');
            }));

            function setupConnection(response: Response, error: Error, url: string) {
                backend.connections.subscribe((connection: MockConnection) => {
                    let expectedUrl = url || 'http://localhost:15000/api/test';
                    if (connection.request.url === expectedUrl) {
                        if (response) {
                            connection.mockRespond(response);
                        } else if (error) {
                            connection.mockError(error);
                        } else {
                            fail('query return expected');
                        }
                    } else {
                        fail('unexpected url: (actual):' + connection.request.url + ' not equal to (expected):' + expectedUrl);
                    }
                });
            }

            function setupSuccessConnection(options: any, url?: string) {
                const responseOptions = new ResponseOptions(options);
                const response = new Response(responseOptions);
                setupConnection(response, null, url);
            }

            function setupErrorConnection(url?: string, error?: any) {
                let err = error || new Error('error');
                setupConnection(null, err, url);
            }

            describe('GET', () => {
                it('should return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    });

                    service.get('test').subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });

                    });
                }));

                it('should call with the access tocken and return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    }, 'http://localhost:15000/api/test');


                    service.get('test', true).subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });
                    });
                }));

                it('should call the correct url with search params and return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    }, 'http://localhost:15000/api/test?toto=titi');

                    service.get('test', false, 'toto=titi').subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });

                    });
                }));

                it('should call the correct url with search params and access token and return the body of the response',
                    async(() => {
                        setupSuccessConnection({
                            body: { test: 'test' },
                            status: 200
                        }, 'http://localhost:15000/api/test?toto=titi');

                        service.get('test', true, 'toto=titi').subscribe((data: Object) => {
                            expect(console.error).not.toHaveBeenCalled();
                            expect(data).toBeDefined();
                            expect(data).toEqual({ test: 'test' });
                        });
                    }));

                it('should return null on error', async(() => {
                    setupErrorConnection();

                    service.get('test').subscribe((data: Object) => {
                        expect(console.error).toHaveBeenCalled();
                        expect(data).toBeNull();
                    });
                }));

                it('should redirect to login if error with 401 status code', async(() => {
                    setupErrorConnection(null, { status: 401 });

                    service.get('test').subscribe(data => {
                        expect(data).toBeNull();
                        expect(routerService.navigate).toHaveBeenCalledWith(['/login']);
                    });
                }));

                it('should throw an error if error code < 500', async(() => {
                    setupErrorConnection(null, { status: 403 });

                    service.get('test').catch(err => {
                        expect(err).toBeDefined();
                        return Observable.of(null);
                    }).subscribe();
                }));
            });

            describe('POST', () => {
                it('should return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    });
                    service.post('test', {}).subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });
                    });
                }));

                it('should call with the access tocken and return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    }, 'http://localhost:15000/api/test');

                    service.post('test', {}, true).subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });
                    });
                }));

                it('should call the correct url with search params and return the body of the response if success', async(() => {
                    setupSuccessConnection({
                        body: { test: 'test' },
                        status: 200
                    }, 'http://localhost:15000/api/test?toto=titi');

                    service.post('test', {}, false, 'toto=titi').subscribe((data: Object) => {
                        expect(console.error).not.toHaveBeenCalled();
                        expect(data).toBeDefined();
                        expect(data).toEqual({ test: 'test' });

                    });
                }));

                it('should call the correct url with search params and access token and return the body of the response',
                    async(() => {
                        setupSuccessConnection({
                            body: { test: 'test' },
                            status: 200
                        }, 'http://localhost:15000/api/test?toto=titi');

                        service.post('test', {}, true, 'toto=titi').subscribe((data: Object) => {
                            expect(console.error).not.toHaveBeenCalled();
                            expect(data).toBeDefined();
                            expect(data).toEqual({ test: 'test' });
                        });
                    }));

                it('should return null on error', async(() => {
                    setupErrorConnection();

                    service.post('test', {}).subscribe((data: Object) => {
                        expect(console.error).toHaveBeenCalled();
                        expect(data).toBeNull();
                    });
                }));

                it('should redirect to login if error with 401 status code', async(() => {
                    setupErrorConnection(null, { status: 401 });

                    service.post('test', {}).subscribe(data => {
                        expect(data).toBeNull();
                        expect(routerService.navigate).toHaveBeenCalledWith(['/login']);
                    });
                }));

                it('should throw an error if error code < 500', async(() => {
                    setupErrorConnection(null, { status: 403 });

                    service.post('test', {}).catch(err => {
                        expect(err).toBeDefined();
                        return Observable.of(null);
                    }).subscribe();
                }));
            });
        });
    });
});
