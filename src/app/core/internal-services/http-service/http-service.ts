import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Request, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../../environments/environment';
import { AuthServiceInternal } from '../auth-service';

enum Method {
    GET,
    POST,
    PUT,
    DELETE
};

@Injectable()
export class HttpService {
    private baseUrl: string = environment.baseUrl;
    private accessToken;

    constructor(
        private http: Http,
        private router: Router,
        private authService: AuthServiceInternal
    ) {
        authService.get().subscribe(token => {
            this.accessToken = token;
        });
    }

    private getUrl(endPoint: string) {
        return this.baseUrl + endPoint;
    }

    private getRequest(method: Method,
        endPoint: string,
        auth: boolean,
        body: Object,
        searchParams: string,
        headers: Headers
    ): Request {
        if (!headers) {
            headers = new Headers();
        }
        if (!headers.has('Content-type')) {
            headers.append('Content-type', 'application/json');
        }
        let urlSearchParams = new URLSearchParams(searchParams);
        if (auth && this.accessToken) {
            urlSearchParams.append('access_token', this.accessToken.id);
        }
        body = typeof body === 'string' ? body : JSON.stringify(body);
        return new Request({
            url: this.getUrl(endPoint),
            method: Method[method],
            headers,
            body,
            params: urlSearchParams
        });
    }

    private execute(req: Request): Observable<any> {
        return this.http
            .request(req)
            .catch((err: Response) => {
                console.error(err);
                if (err.status === 401) {
                    this.authService.removeAccessToken();
                    this.router.navigate(['/login']);
                } else if (err.status < 500) {
                    return Observable.throw(err);
                }
                return Observable.of(null);
            })
            .map((res: Response | null) => {
                if (res !== null) {
                    return res.json();
                } else {
                    return res;
                }
            });
    }

    private doRequest(method: Method,
        endPoint: string,
        auth: boolean,
        body: Object,
        searchParams: string,
        headers?: Headers
    ): Observable<Object> {
        let req: Request = this.getRequest(method, endPoint, auth, body, searchParams, headers);
        return this.execute(req);
    }

    public get(endPoint: string, auth = false, searchParams?: string): Observable<any> {
        return this.doRequest(Method.GET, endPoint, auth, null, searchParams);
    }

    public post(endPoint: string, body: Object, auth = false, searchParams?: string, headers?: Headers): Observable<any> {
        return this.doRequest(Method.POST, endPoint, auth, body, searchParams, headers);
    }

    public put(endPoint: string, body: Object, auth = false, searchParams?: string): Observable<any> {
        return this.doRequest(Method.PUT, endPoint, auth, body, searchParams);
    }

    public delete(endPoint: string, auth = false, searchParams?: string): Observable<any> {
        return this.doRequest(Method.DELETE, endPoint, auth, null, searchParams);
    }

    public directPost(endPoint: string, body: any, options: RequestOptions): Observable<any> {
        let url = this.getUrl(endPoint);
        return this.http.post(url, body, options)
            .map(res => res.json())
            .catch(() => Observable.empty());
    }
}
