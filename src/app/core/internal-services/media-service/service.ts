import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http-service';
import { ImageContent, ImageInfo } from '../../../models';
import { environment } from '../../../../environments/environment';

@Injectable()
export class MediaServiceInternal {
    private CONTAINER = 'trips-images';
    private CONTAINERS_BASE = 'image-containers';

    constructor(
        private httpService: HttpService
    ) {
    }

    public getImageUrl(imageInfo: ImageInfo): string {
        return `${environment.baseUrl}${this.CONTAINERS_BASE}/${imageInfo.container}/download/${imageInfo.name}`;
    }

    public uploadImage(imageContent: ImageContent): Observable<ImageInfo> {
        if (!imageContent ||
            !imageContent.fileList ||
            imageContent.fileList.length === 0) {
            return Observable.empty();
        }
        let formData: FormData = new FormData();
        for (let i = 0; i < imageContent.fileList.length; i++) {
            let file: File = imageContent.fileList[i];
            formData.append('file', file, file.name);
        }
        let headers = new Headers();
        headers.append('Accept', 'application/json');

        let url = `${this.CONTAINERS_BASE}/${this.CONTAINER}/upload`;
        let options = new RequestOptions({ headers });
        return this.httpService.directPost(url, formData, options);
    }

    public deleteImage(imageContent: ImageContent): Observable<void> {
        let url = `${this.CONTAINERS_BASE}/${imageContent.imageInfo.container}/files/${imageContent.imageInfo.name}`;
        return this.httpService.delete(url);
    }
}
