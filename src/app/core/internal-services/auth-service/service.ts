import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AccessToken } from '../../../models/auth';
import { IAppState, AccessTokenActions, STATE_NAMES } from '../../store';

@Injectable()
export class AuthServiceInternal {
    private LOCAL_STORAGE_KEY = 'access_token';

    constructor(
        private store: Store<IAppState>,
        private tokenActions: AccessTokenActions
    ) { }

    private getTokenFromStorage() {
        return localStorage.getItem(this.LOCAL_STORAGE_KEY);
    }

    public saveTokenInStorage(token): void {
        localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(token));
    }

    public deleteTokenFromStorage() {
        localStorage.removeItem(this.LOCAL_STORAGE_KEY);
    }

    public readToken(): AccessToken {
        let tokenStr = this.getTokenFromStorage();
        let token: AccessToken = null;
        try {
            token = JSON.parse(tokenStr);
        } catch (e) {
        } finally {
            if (token === null) {
                token = new AccessToken();
            }
        }
        return token;
    }

    public getAccessTokenFromResponse(res): AccessToken {
        let token: AccessToken = new AccessToken();
        if (res && res.id) {
            token.id = res.id;
            token.userId = res.userId;
        }
        return token;
    }

    public isDefined(token: AccessToken) {
        return !!token && !!token.id;
    }

    public setAccessToken(res: Response, keepLogin: boolean): void {
        this.store.dispatch(this.tokenActions.processLogin(res, keepLogin));
    }

    public removeAccessToken(): void {
        this.store.dispatch(this.tokenActions.delete());
    }

    public get(load = false): Observable<AccessToken> {
        if (load) {
            this.store.dispatch(this.tokenActions.get());
        }
        return this.store.select(STATE_NAMES.ACCESS_TOKEN);
    }
}
