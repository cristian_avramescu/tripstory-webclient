import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http-service';
import { IAppState, TripsActions, TripsSelectors } from '../../store';
import { Trip, BaseContent } from '../../../models';

@Injectable()
export class TripsServiceInternal {

    constructor(
        private httpService: HttpService,
        private tripsActions: TripsActions,
        private store: Store<IAppState>
    ) {
    }

    public fetchAll(userId: string) {
        return this.httpService
            .get('users/' + userId + '/trips', true);
    }

    public fetch(id: string) {
        return this.httpService.get('trips/' + id, true);
    }

    public post(trip: Trip): Observable<Trip> {
        return this.httpService.post('trips', trip, true);
    }

    public postUpdate(trip) {
        return this.httpService.put('trips/' + trip.id, trip, true);
    }

    public serverDelete(id: string) {
        return this.httpService.delete('trips/' + id, true);
    }

    public getAll(): Observable<Trip[]> {
        this.store.dispatch(this.tripsActions.getAll());
        return this.store.select(TripsSelectors.getAll);
    }

    public get(id: string): Observable<Trip> {
        this.store.dispatch(this.tripsActions.select(id));
        return this.getSelected();
    }

    public getSelected(): Observable<Trip> {
        return this.store.select<Trip>(TripsSelectors.get);
    }

    public add(trip: Trip): Observable<string> {
        this.store.dispatch(this.tripsActions.add(trip));
        return this.store
            .select(TripsSelectors.getSelectedId)
            .filter(id => id !== null);
    }

    public update(trip: Trip): void {
        this.store.dispatch(this.tripsActions.update(trip));
    }

    public delete(trip: Trip): void {
        this.store.dispatch(this.tripsActions.delete(trip.id));
    }

    public removeSelected() {
        this.store.dispatch(this.tripsActions.select(null));
    }

    public isDefined(trip: Trip) {
        return trip && trip.id !== null && trip.id !== undefined;
    }

    public getPendingState(): Observable<boolean> {
        return this.store.select(TripsSelectors.getPending);
    }

    public getErrorState(): Observable<any> {
        return this.store.select(TripsSelectors.getError);
    }

    public addContentItem(trip: Trip, contentItem: BaseContent) {
        this.store.dispatch(this.tripsActions.addContentItem(trip, contentItem));
    }

    public updateContentItem(trip: Trip, currentItem: BaseContent, newItem: BaseContent) {
        this.store.dispatch(this.tripsActions.updateContentItem(trip, currentItem, newItem));
    }

    public deleteContentItem(trip: Trip, contentItem: BaseContent) {
        this.store.dispatch(this.tripsActions.deleteContentItem(trip, contentItem));
    }
}
