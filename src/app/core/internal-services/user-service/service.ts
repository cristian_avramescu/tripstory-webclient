import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/throw';
import { HttpService } from '../http-service';
import { AuthServiceInternal } from '../auth-service';
import { User, ICredentials, ISignUpCredentials, AccessToken } from '../../../models';
import { IAppState, UserActions, STATE_NAMES } from '../../store';

@Injectable()
export class UserServiceInternal {
    private basePath = 'users';
    private userObservable: Observable<User>;
    private user: User = new User();
    private token: AccessToken;

    constructor(
        private httpService: HttpService,
        private store: Store<IAppState>,
        private userActions: UserActions,
        private authService: AuthServiceInternal
    ) { }

    private processLoginResponse(res: any, keepLogin: boolean) {
        let user: User = null;
        if (res && res.user && res.user.id) {
            user = new User();
            user.email = res.user.email;
            user.id = res.user.id;
            user.username = res.user.username;
        }
        return { response: res, user, keepLogin };
    }

    private getPath(path?: string): string {
        if (!path) {
            return this.basePath;
        }
        return this.basePath + '/' + path;
    }

    private getLoginCredentials(signUpCredentials: ISignUpCredentials) {
        return {
            email: signUpCredentials.email,
            password: signUpCredentials.password
        };
    }

    public login(credentials: ICredentials, keepLogin: boolean): Observable<boolean> {
        return this.httpService
            .post(this.getPath('login?include=user'), credentials)
            .catch((err) => {
                return Observable.of(false);
            })
            .map((res) => {
                this.store.dispatch(this.userActions.processLogin(this.processLoginResponse(res, keepLogin)));
                return res !== null;
            });
    }

    signUp(signUpCredentials: ISignUpCredentials): Observable<boolean> {
        return this.httpService
            .post(this.getPath(), signUpCredentials)
            .catch(err => {
                return Observable.throw(err);
            })
            .map(() => this.getLoginCredentials(signUpCredentials))
            .mergeMap((credentials: ICredentials) => this.login(credentials, true));
    }

    public logout(): Observable<void> {
        return this.httpService.post(this.getPath('logout'), null, true)
            .map(res => {
                this.store.dispatch(this.userActions.delete());
            });
    }

    public get(load: boolean) {
        if (load) {
            this.load();
        }
        return this.store.select<User>(STATE_NAMES.USER);
    }

    public load() {
        this.store.dispatch(this.userActions.load());
    }

    public isDefined(user: User, onlyValidUsers = true): boolean {
        let isDefined = user && user.id !== null && user.id !== undefined;
        return isDefined && (onlyValidUsers ? user.id !== '-1' : true);
    }

    public loadUser(userId: string): Observable<User> {
        return this.httpService
            .get(this.getPath(userId), true);
    }

    public allowIntoTheApplication(): Observable<boolean> {
        return this.authService
            .get(true)
            .map(token => !!token.id)
            .switchMap(tokenExists => {
                if (tokenExists) {
                    return this.get(true)
                        .filter(user => this.isDefined(user, false))
                        .map(user => this.isDefined(user, true));
                } else {
                    return Observable.of(false);
                }
            });
    }
}
