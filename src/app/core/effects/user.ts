import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../models';
import { UserActionTypes, IAppState, UserActions, AccessTokenActions, TripsActions } from '../store';
import { UserServiceInternal, AuthServiceInternal } from '../internal-services';

@Injectable()
export class UserEffects {
    @Effect() loadUser$ = this.actions$
        .ofType(UserActionTypes.LOAD)
        .withLatestFrom(this.store$)
        .filter(([action, state]) => !this.service.isDefined(state.user, true))
        .switchMap(() => this.authService.get())
        .filter(token => this.authService.isDefined(token))
        .switchMap(token => this.service.loadUser(token.userId))
        .map(user => {
            if (this.service.isDefined(user, false)) {
                return user;
            } else {
                return new User('-1');
            }
        })
        .map(user => this.userActions.set(user));

    @Effect() processLogin$ = this.actions$
        .ofType(UserActionTypes.PROCESS_LOGIN)
        .do(action => this.store$.dispatch(this.userActions.set(action.payload.user)))
        .map(action => this.accessTokenActions.processLogin(action.payload.response, action.payload.keepLogin));

    @Effect() deleteUser$ = this.actions$
        .ofType(UserActionTypes.DELETE)
        .do(() => this.store$.next(this.tripsActions.removeAll()))
        .map(() => this.accessTokenActions.delete());

    constructor(
        private actions$: Actions,
        private service: UserServiceInternal,
        private userActions: UserActions,
        private accessTokenActions: AccessTokenActions,
        private tripsActions: TripsActions,
        private store$: Store<IAppState>,
        private authService: AuthServiceInternal
    ) { }
}
