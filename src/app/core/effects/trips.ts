import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TripsActionTypes, IAppState, TripsActions } from '../store';
import { TripsServiceInternal, UserServiceInternal, MediaServiceInternal } from '../internal-services';
import { User, Trip, ContentType, ImageInfo, ImageContent } from '../../models';

@Injectable()
export class TripsEffects {
    @Effect() getAllTrips$ = this.actions$
        .ofType(TripsActionTypes.GET_ALL)
        .switchMap(() => this.userService.get(false))
        .filter(user => this.userService.isDefined(user))
        .switchMap((user: User) =>
            this.service
                .fetchAll(user.id)
                .map(trips => this.tripsActions.getAllSuccess(trips))
                .catch(error => Observable.of(this.tripsActions.getError(error))));

    @Effect() selectTrip$ = this.actions$
        .ofType(TripsActionTypes.SELECT)
        .map(action => action.payload)
        .filter(id => id !== null && id !== undefined)
        .withLatestFrom(this.store$)
        .filter(([id, state]) => !state.trips.entities[id])
        .switchMap(([id, state]) => this.service
            .fetch(id)
            .map(trip => this.tripsActions.addSuccess(trip))
            .catch(error => Observable.of(this.tripsActions.getError(error))));

    @Effect() addTrip$ = this.actions$
        .ofType(TripsActionTypes.ADD)
        .map(action => action.payload)
        .switchMap((trip: Trip) => this.userService
            .get(false)
            .map(user => {
                trip.ownerId = user.id;
                return trip;
            }))
        .switchMap(trip => this.service
            .post(trip)
            .map(newTrip => this.tripsActions.addSuccess(newTrip))
            .catch(error => Observable.of(this.tripsActions.getError(error))));

    @Effect() addSuccess$ = this.actions$
        .ofType(TripsActionTypes.ADD_SUCCESS)
        .map(action => action.payload.id)
        .map(id => this.tripsActions.select(id));

    @Effect() updateTrip$ = this.actions$
        .ofType(TripsActionTypes.UPDATE)
        .map(action => action.payload)
        .switchMap(trip => this.service
            .postUpdate(trip)
            .map(() => this.tripsActions.updateSuccess(trip))
            .catch(error => Observable.of(this.tripsActions.getError(error))));

    @Effect() deleteTrip$ = this.actions$
        .ofType(TripsActionTypes.DELETE)
        .map(action => action.payload)
        .switchMap(id => this.service
            .serverDelete(id)
            .map(() => this.tripsActions.deleteSuccess(id))
            .catch(error => Observable.of(this.tripsActions.getError(error))));

    @Effect() deleteSuccessTrip$ = this.actions$
        .ofType(TripsActionTypes.DELETE_SUCCESS)
        .map(() => this.tripsActions.select(null));

    @Effect() addContentItem$ = this.actions$
        .ofType(TripsActionTypes.ADD_CONTENT_ITEM)
        .map(action => action.payload)
        .filter(payload => payload.contentItem)
        .switchMap(payload => this.uploadImage(payload))
        .map(payload => {
            let trip = payload.trip;
            if (!trip.content) {
                trip.content = [];
            }
            trip.content.push(payload.contentItem);
            return trip;
        })
        .map(trip => this.tripsActions.update(trip));

    @Effect() updateContentItem$ = this.actions$
        .ofType(TripsActionTypes.UPDATE_CONTENT_ITEM)
        .map(action => action.payload)
        .filter(payload => payload.newItem)
        .do(payload => payload.index = payload.trip.content.indexOf(payload.currentItem))
        .map(payload => {
            return {
                trip: payload.trip,
                contentItem: payload.newItem,
                index: payload.index
            };
        })
        .do(payload => console.log(payload))
        .switchMap(payload => this.deleteImage(payload))
        .switchMap(payload => this.uploadImage(payload))
        .do(payload => console.log(payload))
        .do(payload => payload.trip.content[payload.index] = payload.contentItem)
        .do(payload => console.log(payload))
        .map(payload => payload.trip)
        .map(trip => this.tripsActions.update(trip));

    @Effect() deleteContentItem$ = this.actions$
        .ofType(TripsActionTypes.DELETE_CONTENT_ITEM)
        .map(action => action.payload)
        .do(payload => payload.index = payload.trip.content.indexOf(payload.contentItem))
        .do(payload => payload.trip.content.splice(payload.index, 1))
        .switchMap(payload => this.deleteImage(payload))
        .map(payload => payload.trip)
        .map(trip => this.tripsActions.update(trip));

    constructor(
        private actions$: Actions,
        private service: TripsServiceInternal,
        private tripsActions: TripsActions,
        private store$: Store<IAppState>,
        private userService: UserServiceInternal,
        private mediaService: MediaServiceInternal
    ) { }

    private uploadImage(payload): Observable<any> {
        if (payload.contentItem.type === ContentType.IMAGE &&
            payload.contentItem.fileList) {
            return this.mediaService
                .uploadImage(payload.contentItem)
                .map((res: any) => res.result.files.file[0])
                .map((response: any) => new ImageInfo(response.container, response.name, response.size))
                .map((imageInfo: ImageInfo) => new ImageContent(payload.contentItem.title, imageInfo, payload.contentItem.creationDate))
                .map((contentItem: ImageContent) => Object.assign(payload, { contentItem }));
        }
        return Observable.of(payload);
    }

    private deleteImage(payload): Observable<any> {
        if (payload.contentItem.type === ContentType.IMAGE &&
            payload.contentItem.fileList) {
            return this.mediaService
                .deleteImage(payload.contentItem)
                .map(() => payload);
        } else {
            return Observable.of(payload);
        }
    }
}
