import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AccessTokenActionTypes, IAppState, AccessTokenActions } from '../store';
import { AuthServiceInternal } from '../internal-services';
import { AccessTokenPayload, AccessToken } from '../../models';

@Injectable()
export class AccessTokenEffects {
    @Effect() getAccessToken$ = this.actions$
        .ofType(AccessTokenActionTypes.GET)
        .withLatestFrom(this.store$)
        .filter(([action, state]) => !this.service.isDefined(state.accessToken))
        .map(() => this.service.readToken())
        .filter(token => this.service.isDefined(token))
        .map(token => new AccessTokenPayload(token, false))
        .map(payload => this.accessTokenActions.set(payload));

    @Effect({ dispatch: false }) setAccessToken$ = this.actions$
        .ofType(AccessTokenActionTypes.SET)
        .filter(action => action.payload.keepLogin)
        .do(action => this.service.saveTokenInStorage(action.payload.token));

    @Effect() processLoginResponse$ = this.actions$
        .ofType(AccessTokenActionTypes.PROCESS_LOGIN)
        .map(action => new AccessTokenPayload(
            this.service.getAccessTokenFromResponse(action.payload.response),
            !!action.payload.keepLogin))
        .map(payload => this.accessTokenActions.set(payload));

    @Effect({ dispatch: false }) deleteAccessToken$ = this.actions$
        .ofType(AccessTokenActionTypes.DELETE)
        .do(() => this.service.deleteTokenFromStorage());

    constructor(
        private actions$: Actions,
        private service: AuthServiceInternal,
        private store$: Store<IAppState>,
        private accessTokenActions: AccessTokenActions
    ) { }
}
