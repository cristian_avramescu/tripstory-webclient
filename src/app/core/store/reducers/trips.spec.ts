import { Trip } from '../../../models';
import { TripsActionTypes } from '../actions';
import * as fromTrips from './trips';

describe('CoreModule', () => {
    describe('Store', () => {
        describe('Reducers', () => {
            describe('Trips', () => {
                let initialState: fromTrips.State;
                let trip: Trip;

                beforeEach(() => {
                    initialState = {
                        ids: [],
                        entities: {},
                        selected: null,
                        pending: false,
                        error: null
                    };
                    trip = new Trip();
                    trip.id = '1';
                    trip.name = 'test';
                });

                it('should return a new state with the trip inserted', () => {
                    let action = {
                        type: TripsActionTypes.ADD_SUCCESS,
                        payload: trip
                    };
                    let result = fromTrips.reducer(initialState, action);
                    expect(result).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                    expect(initialState).toEqual({
                        ids: [],
                        entities: {},
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should not duplicate trips if inserting the same one twice', () => {
                    let action = {
                        type: TripsActionTypes.ADD_SUCCESS,
                        payload: trip
                    };
                    let result = fromTrips.reducer(initialState, action);
                    expect(result).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                    expect(initialState).toEqual({
                        ids: [],
                        entities: {},
                        selected: null,
                        pending: false,
                        error: null
                    });
                    let result2 = fromTrips.reducer(result, action);
                    expect(result2).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should return a new state with the updated trip', () => {
                    let newTrip: Trip = new Trip();
                    newTrip.id = '1';
                    newTrip.name = 'toto';
                    let action = {
                        type: TripsActionTypes.UPDATE_SUCCESS,
                        payload: newTrip
                    };
                    let state = {
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['1'],
                        entities: { '1': newTrip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should return a new state with the list of trips inserted', () => {
                    let trip1: Trip = new Trip();
                    trip1.id = '2';
                    trip1.name = 't2';
                    let trip2: Trip = new Trip();
                    trip2.id = '3';
                    trip2.name = 't3';
                    let action = {
                        type: TripsActionTypes.GET_ALL_SUCCESS,
                        payload: [trip1, trip2]
                    };
                    let state = {
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['1', '2', '3'],
                        entities: {
                            '1': trip,
                            '2': trip1,
                            '3': trip2
                        },
                        selected: null,
                        pending: false,
                        error: null
                    });
                    expect(state).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should not insert duplicates from a list', () => {
                    let trip1: Trip = new Trip();
                    trip1.id = '2';
                    trip1.name = 't2';
                    let trip2: Trip = new Trip();
                    trip2.id = '3';
                    trip2.name = 't3';
                    let trip3: Trip = new Trip();
                    trip3.id = '3';
                    trip3.name = 't3';
                    let action = {
                        type: TripsActionTypes.GET_ALL_SUCCESS,
                        payload: [trip, trip1, trip2, trip3]
                    };
                    let state = {
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['1', '2', '3'],
                        entities: {
                            '1': trip,
                            '2': trip1,
                            '3': trip2
                        },
                        selected: null,
                        pending: false,
                        error: null
                    });
                    expect(state).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should return a new state with the selected id', () => {
                    let action = {
                        type: TripsActionTypes.SELECT,
                        payload: '1'
                    };
                    let state = {
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: '1',
                        pending: false,
                        error: null
                    });
                });

                it('should return a new state with the deleted trip', () => {
                    let trip1: Trip = new Trip();
                    trip1.id = '2';
                    trip1.name = 't2';
                    let action = {
                        type: TripsActionTypes.DELETE_SUCCESS,
                        payload: '1'
                    };
                    let state = {
                        ids: ['1', '2'],
                        entities: {
                            '1': trip,
                            '2': trip1
                        },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['2'],
                        entities: { '2': trip1 },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });

                it('should return the state if action unknown', () => {
                    let action = {
                        type: 'some action',
                        payload: '1'
                    };
                    let state = {
                        ids: ['1'],
                        entities: {
                            '1': trip
                        },
                        selected: null,
                        pending: false,
                        error: null
                    };
                    let result = fromTrips.reducer(state, action);
                    expect(result).toEqual({
                        ids: ['1'],
                        entities: { '1': trip },
                        selected: null,
                        pending: false,
                        error: null
                    });
                });
            });
        });
    });
});
