import { AccessToken } from '../../../models';
import { AccessTokenActionTypes } from '../actions';
import { AccessTokenReducer } from './access.token';

describe('CoreModule', () => {
    describe('Store', () => {
        describe('Reducers', () => {
            describe('Access token', () => {
                it('should return a copy of the passed access token', () => {
                    let token = new AccessToken();
                    token.id = '1';
                    let result = AccessTokenReducer(new AccessToken(), {
                        type: AccessTokenActionTypes.SET,
                        payload: { token }
                    });
                    expect(result).toBeDefined();
                    expect(result.id).toEqual('1');
                    token.id = '2';
                    expect(result.id).toEqual('1');
                });
                it('should return the state if action unknown', () => {
                    let token = new AccessToken();
                    token.id = '1';
                    let result = AccessTokenReducer(token, {
                        type: 'unknown action type',
                        payload: token
                    });
                    expect(result).toBeDefined();
                    expect(result.id).toEqual('1');
                    token.id = '2';
                    expect(result.id).toEqual('2');
                });
            });
        });
    });
});
