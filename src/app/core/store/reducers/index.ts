import { compose } from '@ngrx/core/compose';
import { combineReducers } from '@ngrx/store';
import { createSelector } from 'reselect';
import { storeLogger } from 'ngrx-store-logger';
import { UserReducer, UserState } from './user';
import { AccessTokenReducer, AccessTokenState } from './access.token';
import { environment } from '../../../../environments/environment';

import * as fromTrips from './trips';

export interface IAppState {
    user: UserState;
    accessToken: AccessTokenState;
    trips: fromTrips.State;
};

export function reducers() {
    let reducersDef = {
        user: UserReducer,
        accessToken: AccessTokenReducer,
        trips: fromTrips.reducer
    };

    let returnFunction;
    if (environment.production) {
        returnFunction = compose(combineReducers)(reducersDef);
    } else {
        returnFunction = compose(storeLogger(), combineReducers)(reducersDef);
    }
    return returnFunction.apply(this, arguments);
}

export const STATE_NAMES = {
    USER: 'user',
    ACCESS_TOKEN: 'accessToken'
};

const getTripsState = (state: IAppState) => state.trips;

export const TripsSelectors = {
    getAll: createSelector(getTripsState, fromTrips.getAll),
    get: createSelector(getTripsState, fromTrips.getSelected),
    getPending: createSelector(getTripsState, fromTrips.getPending),
    getSelectedId: createSelector(getTripsState, fromTrips.getSelectedId),
    getError: createSelector(getTripsState, fromTrips.getError)
};
