import { Action } from '@ngrx/store';

import { User } from '../../../models';
import { UserActionTypes } from '../actions';

export type UserState = User;

const initialState: UserState = new User();

export function UserReducer(state = initialState, action: Action): UserState {
    switch (action.type) {
        case UserActionTypes.SET:
            return Object.assign({}, action.payload);
        case UserActionTypes.DELETE:
            return initialState;
        default:
            return state;
    }
}
