import { Action } from '@ngrx/store';

import { AccessToken } from '../../../models';
import { AccessTokenActions, AccessTokenActionTypes } from '../actions';

export type AccessTokenState = AccessToken;

const initialState: AccessTokenState = new AccessToken();

export function AccessTokenReducer(state = initialState, action: Action): AccessTokenState {
    switch (action.type) {
        case AccessTokenActionTypes.SET:
            return Object.assign({}, action.payload.token);
        case AccessTokenActionTypes.DELETE:
            return initialState;
        default:
            return state;
    }
}
