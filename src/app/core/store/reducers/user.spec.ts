import { User } from '../../../models';
import { UserActionTypes } from '../actions';
import { UserReducer } from './user';

describe('CoreModule', () => {
    describe('Store', () => {
        describe('Reducers', () => {
            describe('User', () => {
                it('should return a copy of the passed user', () => {
                    let user = new User();
                    user.id = '1';
                    user.username = 'test';
                    let result = UserReducer(new User(), {
                        type: UserActionTypes.SET,
                        payload: user
                    });
                    expect(result).toBeDefined();
                    expect(result.id).toEqual('1');
                    user.id = '2';
                    expect(result.id).toEqual('1');
                });
                it('should return the state if action unknown', () => {
                    let user = new User();
                    user.id = '1';
                    user.username = 'test';
                    let result = UserReducer(user, {
                        type: 'unknown action type',
                        payload: user
                    });
                    expect(result).toBeDefined();
                    expect(result.id).toEqual('1');
                    user.id = '2';
                    expect(result.id).toEqual('2');
                });
            });
        });
    });
});
