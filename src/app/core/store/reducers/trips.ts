import { Action } from '@ngrx/store';
import { createSelector } from 'reselect';
import { Trip } from '../../../models';
import { TripsActions, TripsActionTypes } from '../actions';

export type State = {
    ids: string[];
    entities: { [id: string]: Trip };
    selected: string;
    pending: boolean;
    error: any;
};

const initialState: State = {
    ids: [],
    entities: {},
    selected: null,
    pending: false,
    error: null
};

function doDelete(state = initialState, action: Action): State {
    let entities = Object.assign({}, state.entities);
    delete entities[action.payload];
    let newState = {
        ids: state.ids.filter(id => id !== action.payload),
        entities: entities,
        pending: false
    };
    return Object.assign({}, state, newState);
}

function insert(state = initialState, action: Action): State {
    let newState = {
        ids: [...state.ids],
        entities: Object.assign({}, state.entities, { [action.payload.id]: action.payload }),
        pending: false
    };
    if (newState.ids.indexOf(action.payload.id) < 0) {
        newState.ids.push(action.payload.id);
    }
    return Object.assign({}, state, newState);
}

function select(state = initialState, action: Action): State {
    let newState = {
        selected: action.payload,
        error: null
    };
    return Object.assign({}, state, newState);
}

function insertList(state = initialState, action: Action): State {
    let newIds: string[] = [];
    let newEntities: { [id: string]: Trip } = {};
    action.payload.map(trip => {
        if (state.ids.indexOf(trip.id) < 0 && newIds.indexOf(trip.id) < 0) {
            newIds.push(trip.id);
        }
        newEntities[trip.id] = trip;
    });
    let newState = {
        ids: [...state.ids, ...newIds],
        entities: Object.assign({}, state.entities, newEntities)
    };
    return Object.assign({}, state, {
        ids: [...state.ids, ...newIds],
        entities: Object.assign({}, state.entities, newEntities),
        pending: false,
        error: null
    });
}

function update(state = initialState, action: Action): State {
    let newState = {
        entities: Object.assign({}, state.entities,
            { [action.payload.id]: Trip.clone(action.payload) }),
        pending: false
    };
    return Object.assign({}, state, newState);
}

export function reducer(state = initialState, action: Action): State {
    switch (action.type) {
        case TripsActionTypes.GET_ALL:
        case TripsActionTypes.ADD:
        case TripsActionTypes.DELETE:
        case TripsActionTypes.UPDATE:
            return Object.assign({}, state, { pending: true, error: null });

        case TripsActionTypes.ADD_SUCCESS:
        case TripsActionTypes.SELECT_SUCCESS:
            return insert(state, action);

        case TripsActionTypes.GET_ALL_SUCCESS:
            return insertList(state, action);

        case TripsActionTypes.SELECT:
            return select(state, action);

        case TripsActionTypes.DELETE_SUCCESS:
            return doDelete(state, action);

        case TripsActionTypes.UPDATE_SUCCESS:
            return update(state, action);

        case TripsActionTypes.GET_ERROR:
            return Object.assign({}, state, { pending: false, error: action.payload });

        case TripsActionTypes.REMOVE_ALL:
            return initialState;

        default:
            return state;
    }
}

export const getEntities = (state: State) => state.entities;

export const getIds = (state: State) => state.ids;

export const getSelectedId = (state: State) => state.selected;

export const getSelected = createSelector(getEntities, getSelectedId, (entities, id) => entities[id]);

export const getAll = createSelector(getEntities, getIds, (entities, ids) => ids.map(id => entities[id]));

export const getPending = (state: State) => state.pending;

export const getError = (state: State) => state.error;
