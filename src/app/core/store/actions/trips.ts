import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Trip, BaseContent } from '../../../models';

export const TripsActionTypes = {
    ADD: 'ADD_TRIP',
    ADD_SUCCESS: 'ADD_SUCCESS_TRIP',

    GET_ALL: 'GET_ALL_TRIPS',
    GET_ALL_SUCCESS: 'GET_ALL_TRIPS_SUCCESS',

    UPDATE: 'UPDATE_TRIP',
    UPDATE_SUCCESS: 'UPDATE_SUCCESS_TRIP',

    ADD_CONTENT_ITEM: 'ADD_CONTENT_ITEM_TRIP',
    UPDATE_CONTENT_ITEM: 'UPDATE_CONTENT_ITEM_TRIP',
    DELETE_CONTENT_ITEM: 'DELETE_CONTENT_ITEM_TRIP',

    DELETE: 'DELETE_TRIP',
    DELETE_SUCCESS: 'DELETE_SUCCESS_TRIP',

    SELECT: 'SELECT_TRIP',
    SELECT_SUCCESS: 'SELECT_SUCCESS',

    REMOVE_ALL: 'REMOVE_ALL_TRIPS',

    GET_ERROR: 'GET_TRIPS_ERROR'
};

@Injectable()
export class TripsActions {
    add(trip: Trip): Action {
        return {
            type: TripsActionTypes.ADD,
            payload: trip
        };
    }
    addSuccess(trip: Trip): Action {
        return {
            type: TripsActionTypes.ADD_SUCCESS,
            payload: trip
        };
    }

    update(trip: Trip): Action {
        return {
            type: TripsActionTypes.UPDATE,
            payload: trip
        };
    }
    updateSuccess(trip: Trip): Action {
        return {
            type: TripsActionTypes.UPDATE_SUCCESS,
            payload: trip
        };
    }

    delete(id: string): Action {
        return {
            type: TripsActionTypes.DELETE,
            payload: id
        };
    }
    deleteSuccess(id: string): Action {
        return {
            type: TripsActionTypes.DELETE_SUCCESS,
            payload: id
        };
    }

    getAll(): Action {
        return {
            type: TripsActionTypes.GET_ALL
        };
    }
    getAllSuccess(trips: Trip[]): Action {
        return {
            type: TripsActionTypes.GET_ALL_SUCCESS,
            payload: trips
        };
    }

    select(id: string): Action {
        return {
            type: TripsActionTypes.SELECT,
            payload: id
        };
    }
    selectSuccess(id: string): Action {
        return {
            type: TripsActionTypes.SELECT_SUCCESS,
            payload: id
        };
    }

    getError(error: any): Action {
        return {
            type: TripsActionTypes.GET_ERROR,
            payload: error
        };
    }

    removeAll(): Action {
        return {
            type: TripsActionTypes.REMOVE_ALL
        };
    }

    addContentItem(trip: Trip, contentItem: BaseContent) {
        return {
            type: TripsActionTypes.ADD_CONTENT_ITEM,
            payload: {
                trip,
                contentItem
            }
        };
    }

    updateContentItem(trip: Trip, currentItem: BaseContent, newItem: BaseContent) {
        return {
            type: TripsActionTypes.UPDATE_CONTENT_ITEM,
            payload: {
                trip,
                currentItem,
                newItem
            }
        };
    }

    deleteContentItem(trip: Trip, contentItem: BaseContent) {
        return {
            type: TripsActionTypes.DELETE_CONTENT_ITEM,
            payload: {
                trip,
                contentItem
            }
        };
    }
}
