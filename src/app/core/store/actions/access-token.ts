import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { AccessTokenPayload } from '../../../models';

export const AccessTokenActionTypes = {
    SET: 'SET_ACCESS_TOKEN',
    GET: 'GET_ACCESS_TOKEN',
    PROCESS_LOGIN: 'PROCESS_LOGIN_ACCESS_TOKEN',
    DELETE: 'DELETE_ACCESS_TOKEN'
};

@Injectable()
export class AccessTokenActions {
    set(token: AccessTokenPayload): Action {
        return {
            type: AccessTokenActionTypes.SET,
            payload: token
        };
    }

    get(): Action {
        return {
            type: AccessTokenActionTypes.GET
        };
    }

    processLogin(response: any, keepLogin: boolean): Action {
        return {
            type: AccessTokenActionTypes.PROCESS_LOGIN,
            payload: {
                response,
                keepLogin
            }
        };
    }

    delete(): Action {
        return {
            type: AccessTokenActionTypes.DELETE
        };
    }
}
