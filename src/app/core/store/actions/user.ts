import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from '../../../models';

export const UserActionTypes = {
    SET: 'SET_USER',
    LOAD: 'LOAD_USER',
    PROCESS_LOGIN: 'PROCESS_LOGIN_USER',
    DELETE: 'DELETE_USER'
};

@Injectable()
export class UserActions {
    set(user: User): Action {
        return {
            type: UserActionTypes.SET,
            payload: user
        };
    }

    load() {
        return {
            type: UserActionTypes.LOAD
        };
    }

    processLogin(payload: any) {
        return {
            type: UserActionTypes.PROCESS_LOGIN,
            payload: payload
        };
    }

    delete() {
        return {
            type: UserActionTypes.DELETE
        };
    }
}
