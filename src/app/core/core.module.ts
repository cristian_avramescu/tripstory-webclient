import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    UsersService,
    AuthGuard,
    LoginGuard,
    TripsService,
    UserResolver,
    MediaService
} from './services';
import {
    HttpService,
    AuthServiceInternal,
    TripsServiceInternal,
    UserServiceInternal,
    MediaServiceInternal
} from './internal-services';
import {
    AccessTokenActions,
    UserActions,
    UserActionTypes,
    TripsActions,
    reducers
} from './store';
import {
    TripsEffects,
    AccessTokenEffects,
    UserEffects
} from './effects';

@NgModule({
    imports: [
        StoreModule.provideStore(reducers),
        HttpModule,
        EffectsModule.run(TripsEffects),
        EffectsModule.run(AccessTokenEffects),
        EffectsModule.run(UserEffects)
    ],
    exports: [
        FormsModule,
        RouterModule,
        MaterialModule,
        ReactiveFormsModule,
        CommonModule,
        FlexLayoutModule,
        BrowserAnimationsModule
    ],
    declarations: [],
    providers: [
        AccessTokenActions,
        UserActions,
        TripsActions,
        HttpService,
        UsersService,
        AuthGuard,
        LoginGuard,
        TripsService,
        UserResolver,
        AuthServiceInternal,
        TripsServiceInternal,
        UserServiceInternal,
        MediaService,
        MediaServiceInternal
    ]
})
export class CoreModule { }

export {
    AccessTokenActions,
    AccessTokenActionTypes,
    UserActions,
    UserActionTypes,
    TripsActions,
    IAppState,
    STATE_NAMES,
    TripsSelectors
} from './store';
