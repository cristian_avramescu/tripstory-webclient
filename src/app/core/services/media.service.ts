import { Injectable } from '@angular/core';
import { ImageInfo } from '../../models';
import { MediaServiceInternal } from '../internal-services';

@Injectable()
export class MediaService {

    constructor(
        private internalService: MediaServiceInternal
    ) { }

    public getImageUrl(imageInfo: ImageInfo): string {
        return this.internalService.getImageUrl(imageInfo);
    }
}
