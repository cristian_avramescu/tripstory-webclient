import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User, ICredentials, ISignUpCredentials } from '../../models';
import { UserServiceInternal } from '../internal-services';

@Injectable()
export class UsersService {

    constructor(
        private internalService: UserServiceInternal
    ) {}

    login(credentials: ICredentials, keepLogin: boolean): Observable<boolean> {
        return this.internalService.login(credentials, keepLogin);
    }

    signUp(signUpCredentials: ISignUpCredentials): Observable<boolean> {
        return this.internalService.signUp(signUpCredentials);
    }

    logout(): Observable<void> {
        return this.internalService.logout();
    }

    public getUser(load = false): Observable<User> {
        return this.internalService.get(load);
    }
}
