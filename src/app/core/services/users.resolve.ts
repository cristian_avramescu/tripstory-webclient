import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models';
import { UsersService } from './users.service';

@Injectable()
export class UserResolver implements Resolve<User> {
    constructor(private service: UsersService) { }

    resolve(): Observable<any> | Promise<any> | any {
        return this.service
            .getUser(true)
            .do(user => console.log('user'))
            .do(user => console.log(user))
            // .filter(user => !!(user && user.id))
            .take(1);
    }
}
