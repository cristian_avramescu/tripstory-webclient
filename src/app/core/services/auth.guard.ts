import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserServiceInternal } from '../internal-services';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserServiceInternal
  ) { }

  canActivate(): Observable<boolean> {
    return this.userService
      .allowIntoTheApplication()
      .do(allow => {
        if (!allow) {
          this.router.navigate(['/login']);
        }
      }).take(1);
  }
}
