import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Trip, BaseContent } from '../../models';
import { TripsServiceInternal } from '../internal-services';

@Injectable()
export class TripsService {
    constructor(
        private internalService: TripsServiceInternal
    ) {
    }

    public getAll(): Observable<Trip[]> {
        return this.internalService.getAll();
    }

    public get(id: string): Observable<Trip> {
        return this.internalService.get(id);
    }

    public getSelected(): Observable<Trip> {
        return this.internalService.getSelected();
    }

    public add(trip: Trip): Observable<string> {
        return this.internalService.add(trip);
    }

    public update(trip: Trip): void {
        this.internalService.update(trip);
    }

    public delete(trip: Trip): void {
        this.internalService.delete(trip);
    }

    public removeSelected(): void {
        this.internalService.removeSelected();
    }

    public isDefined(trip: Trip): boolean {
        return this.internalService.isDefined(trip);
    }

    public getPending(): Observable<boolean> {
        return this.internalService.getPendingState();
    }

    public getError(): Observable<any> {
        return this.internalService.getErrorState();
    }

    public addContentItem(trip: Trip, contentItem: BaseContent): void {
        this.internalService.addContentItem(trip, contentItem);
    }

    public updateContentItem(trip: Trip, currentItem: BaseContent, newItem: BaseContent): void {
        this.internalService.updateContentItem(trip, currentItem, newItem);
    }

    public deleteContentItem(trip: Trip, contentItem: BaseContent): void {
        this.internalService.deleteContentItem(trip, contentItem);
    }
}
