import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserServiceInternal } from '../internal-services';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private router: Router,
    private userService: UserServiceInternal
  ) { }

  canActivate(): Observable<boolean> {
    return this.userService
      .allowIntoTheApplication()
      .map(allow => !allow)
      .do(allow => {
        if (!allow) {
          this.router.navigate(['/']);
        }
      }).take(1);
  }
}
