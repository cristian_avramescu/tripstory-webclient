import { NgModule, Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

@Component({
  template: ''
})
export class DummyComponent {
}

@NgModule({
  declarations: [
    DummyComponent
  ],
  imports: [
    RouterTestingModule.withRoutes([
        { path: 'login', component: DummyComponent }
    ])
  ]
})
export class RouterModuleTesting { }
