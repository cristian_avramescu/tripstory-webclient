import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class RouterMock {
  navigate = jasmine.createSpy('navigate');
  routerState = {};
}

export class EmptyMock { }

export class StoreMock {
  data = {};
  select;
  dispatch;
  set(data) {
    this.data = data;
  }
  constructor() {
    this.select = jasmine.createSpy('select').and.returnValue(Observable.of(this.data));
    this.dispatch = jasmine.createSpy('dispatch').and.callFake((data: Action) => {
      this.data = data.payload;
    });
  }
}
