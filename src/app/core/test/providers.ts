import { BaseRequestOptions, Http, XHRBackend, ResponseOptions, Response } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Store } from '@ngrx/store';
import { StoreMock } from './mocks';

export const HTTP_SERVICE_PROVIDERS = [
    BaseRequestOptions,
    MockBackend,
    {
        deps: [
            MockBackend,
            BaseRequestOptions
        ],
        provide: Http,
        useFactory: (xhrBackend: XHRBackend, defaultOptions: BaseRequestOptions) => {
            return new Http(xhrBackend, defaultOptions);
        }
    }
];

export const STORE_PROVIDERS = [
    { provide: Store, useClass: StoreMock }
];
