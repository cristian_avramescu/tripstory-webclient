import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard, LoginGuard } from './core';

export const routes: Routes = [];

export const AppRouting = RouterModule.forRoot(routes);
