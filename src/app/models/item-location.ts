export class ItemLocation {
    public lat: number;
    public lng: number;
    public address?: string;
    public placeName?: string;
    public placeId?: string;
    public city?: string;
    public country?: string;

    constructor(lat: number, lng: number) {
        this.lat = lat;
        this.lng = lng;
    }
}
