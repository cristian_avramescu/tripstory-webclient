import { BaseContent, ContentType } from './base-content';
import { ItemLocation } from './item-location';

export class NoteContent extends BaseContent {
    constructor(title?: string, public content?: string, creationDate?: Date) {
        super(creationDate, title);
        this.type = ContentType.NOTE;
        if (this.content === undefined) {
            this.content = '';
        }
    }
}
