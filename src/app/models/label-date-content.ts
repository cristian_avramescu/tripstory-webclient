import { BaseContent, ContentType } from './base-content';

export class LabelDateContent extends BaseContent {
    constructor(date: Date) {
        super(date);
        this.type = ContentType.LABEL_DATE;
    }
}
