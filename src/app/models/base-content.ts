import { ItemLocation } from './item-location';

export enum ContentType {
    LABEL_DATE,
    NOTE,
    IMAGE
}

export class BaseContent {
    creationDate: Date;
    type: ContentType;
    location?: ItemLocation;

    constructor(creationDate: Date, public title?: string) {
        if (creationDate) {
            this.creationDate = creationDate;
        } else {
            this.creationDate = new Date();
        }
    }
}
