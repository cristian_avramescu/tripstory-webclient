import { BaseContent } from './base-content';

export class Trip {
    name: string = null;
    id: string = null;
    description?: string = null;
    ownerId: string = null;
    content: BaseContent[] = [];

    static clone(trip: Trip): Trip {
        let newTrip = new Trip();
        Object.assign(newTrip, trip);
        return newTrip;
    }
}
