import { BaseContent, ContentType } from './base-content';
import { ItemLocation } from './item-location';

export class ImageInfo {
    constructor(
        public container?: string,
        public name?: string,
        public size?: number
    ) { }
}

export class ImageContent extends BaseContent {
    public fileList: FileList;
    constructor(title?: string, public imageInfo?: ImageInfo, creationDate?: Date) {
        super(creationDate, title);
        this.type = ContentType.IMAGE;
    }
}
