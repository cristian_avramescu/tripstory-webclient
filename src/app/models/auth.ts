export interface ICredentials {
    email: string;
    password: string;
}

export interface ISignUpCredentials extends ICredentials {
    username: string;
}

export class AccessToken {
    id: string = null;
    userId: string = null;
}
