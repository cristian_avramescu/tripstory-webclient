export class User {
    username: string = null;
    email: string = null;

    constructor(public id?: string) { }
}
