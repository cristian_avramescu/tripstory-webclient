import { AccessToken } from './auth';

export class AccessTokenPayload {
    constructor(public token: AccessToken, public keepLogin: boolean) { }
}
