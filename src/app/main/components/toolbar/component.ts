import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../../models';

export enum ToolbarActions {
    NavigateHome,
    Logout
}

@Component({
    selector: 'app-toolbar',
    styleUrls: ['./style.scss'],
    templateUrl: 'component.html'
})
export class ToolbarComponent {
    public Actions = ToolbarActions;

    @Input()
    user: User;

    @Output()
    action: EventEmitter<ToolbarActions> = new EventEmitter<ToolbarActions>();

    constructor() { }

    onAction(action: ToolbarActions) {
        this.action.emit(action);
    }
}
