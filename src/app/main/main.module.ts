import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRouting } from './main.routing';
import { TripsModule } from './trips';
import { ToolbarComponent } from './components';

@NgModule({
    imports: [
        MainRouting,
        TripsModule
    ],
    exports: [MainComponent],
    declarations: [
        MainComponent,
        ToolbarComponent
    ]
})
export class MainModule { }
