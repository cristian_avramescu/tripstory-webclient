import { Pipe, PipeTransform } from '@angular/core';
import { BaseContent, LabelDateContent, NoteContent, ContentType } from '../../../models';

@Pipe({
    name: 'contentsort',
    pure: false
})
export class ContentSortPipe implements PipeTransform {
    temp: BaseContent[] = [];

    transform(content: BaseContent[]): BaseContent[] {
        this.temp.length = 0;
        if (content) {
            content.forEach(item => {
                this.temp.push(item);
            });
            this.temp.map(item => {
                item.creationDate = new Date(item.creationDate);
            });
            this.temp = this.temp.sort((c1: BaseContent, c2: BaseContent) => {
                return c1.creationDate.getTime() - c2.creationDate.getTime();
            });
            let currentDate: Date = new Date(0);
            currentDate.setHours(12);
            let currentIndex = 0;
            while (this.temp[currentIndex]) {
                let currentItemDate = this.temp[currentIndex].creationDate;
                let currentItemType = this.temp[currentIndex].type;
                if (!this.compareDates(currentItemDate, currentDate)) {
                    currentDate.setFullYear(currentItemDate.getFullYear());
                    currentDate.setMonth(currentItemDate.getMonth());
                    currentDate.setDate(currentItemDate.getDate());
                    if (currentItemType !== ContentType.LABEL_DATE) {
                        let date = new Date(currentDate);
                        let labelDateContent = new LabelDateContent(date);
                        this.temp.splice(currentIndex, 0, labelDateContent);
                        currentIndex++;
                    }
                }
                currentIndex++;
            }
        }
        return this.temp;
    }

    private compareDates(date1: Date, date2: Date): boolean {
        return date1.getFullYear() === date2.getFullYear() &&
            date1.getMonth() === date2.getMonth() &&
            date1.getDate() === date2.getDate();
    }

}
