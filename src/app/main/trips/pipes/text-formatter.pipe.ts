import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'textFormat'
})
export class TextFormatPipe implements PipeTransform {

    transform(text: string) {
        if (text === null || text === undefined) {
            return text;
        }
        return text.replace(/\r?\n/g, '<br />');
    }

}
