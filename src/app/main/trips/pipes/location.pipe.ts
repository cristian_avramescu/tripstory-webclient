import { Pipe, PipeTransform } from '@angular/core';
import { ItemLocation } from '../../../models';

@Pipe({
    name: 'location'
})
export class LocationPipe implements PipeTransform {
    private defaultSelectorMessage = 'Pick location';
    private defaultMessage = 'No location set';

    transform(location: ItemLocation, type: string): string {
        if (!location || typeof location.lat === 'undefined') {
            if (type === 'selector') {
                return this.defaultSelectorMessage;
            }
            return this.defaultMessage;
        }
        if (location.placeName) {
            let result = location.placeName;
            if (location.city) {
                result += ', ';
                result += location.city;
            }
            if (location.country) {
                result += ', ';
                result += location.country;
            }
            return result;
        }
        if (location.address) {
            return location.address;
        }
        return 'Lat: ' + location.lat + ', lon: ' + location.lng;
    }

}
