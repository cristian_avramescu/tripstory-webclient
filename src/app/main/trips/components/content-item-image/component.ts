import { Component, Input, OnInit } from '@angular/core';
import { ImageContent, ImageInfo } from '../../../../models';
import { MediaService } from '../../../../core';

@Component({
    selector: 'app-content-image',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentItemImageComponent implements OnInit {
    public imageSource: string;

    @Input() content: ImageContent;

    constructor(private mediaService: MediaService) { }

    ngOnInit() {
        if (this.content && this.content.imageInfo) {
            this.imageSource = this.mediaService.getImageUrl(this.content.imageInfo);
        }
    }
}
