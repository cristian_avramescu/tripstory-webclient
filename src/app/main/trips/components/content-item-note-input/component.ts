import { Component, Input } from '@angular/core';
import { NoteContent } from '../../../../models';

@Component({
    selector: 'app-content-note-input',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentItemNoteInputComponent {
    @Input() public note: NoteContent;
    constructor() { }
}
