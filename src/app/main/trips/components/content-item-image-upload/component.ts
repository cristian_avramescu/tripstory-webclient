import { Component, Input, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ImageContent } from '../../../../models';
import { MediaService } from '../../../../core';

@Component({
    selector: 'app-content-image-upload',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentItemImageUploadComponent implements OnInit {
    @ViewChild('fileElem') fileElemRef: ElementRef;
    @ViewChild('preview') previewElementRef: ElementRef;

    @Input() content: ImageContent;

    public hasImage = false;

    constructor(
        private mediaService: MediaService
    ) { }

    ngOnInit() {
        if (this.content.imageInfo) {
            this.hasImage = true;
            this.previewElementRef.nativeElement.src =
                this.mediaService.getImageUrl(this.content.imageInfo);
        }
    }

    handleClick() {
        console.log('here');
        this.fileElemRef.nativeElement.click();
    }

    handleFiles($event) {
        let imageType = /^image\//;
        let files: FileList = $event.srcElement.files;
        for (let i = 0; i < files.length; i++) {
            let file: File = files[i];

            if (!imageType.test(file.type)) {
                continue;
            }

            let previewElem = this.previewElementRef.nativeElement;
            previewElem.file = file;

            let reader = new FileReader();
            reader.onload = (e: any) => {
                previewElem.src = e.target.result;
                this.hasImage = true;
            };
            reader.readAsDataURL(file);
        }
        this.content.fileList = files;
    }

    removeSelected() {
        this.fileElemRef.nativeElement.value = '';
        this.content.fileList = null;
        this.hasImage = false;
    }
}
