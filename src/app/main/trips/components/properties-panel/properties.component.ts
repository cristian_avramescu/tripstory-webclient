import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Trip } from '../../../../models';

@Component({
    selector: 'app-trip-properties',
    templateUrl: 'properties.component.html',
    styleUrls: ['./properties.style.scss']
})
export class TripPropertiesPanelComponent implements OnInit {
    public tripForm: FormGroup;
    public submitted: boolean;

    @Input()
    trip: Trip;

    @Output()
    onSubmit: EventEmitter<Trip> = new EventEmitter<Trip>();

    @Output()
    delete = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder
    ) {
        this.tripForm = formBuilder.group({
            name: ['', [<any>Validators.required]],
            description: ['']
        });
    }

    public ngOnInit() {
        this.tripForm.controls['name'].setValue(this.trip.name);
        this.tripForm.controls['description'].setValue(this.trip.description);
    }

    public onsubmit(value: Trip, valid: boolean) {
        this.submitted = true;
        if (valid) {
            this.onSubmit.emit(value);
        }
    }

    public ondelete() {
        this.delete.emit();
    }

    isControlInError(control) {
        return !control.valid && (control.touched || this.submitted);
    }
}
