import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Trip } from '../../../../models';

@Component({
    selector: 'app-trip-create',
    templateUrl: 'create.component.html',
    styleUrls: ['./create.style.scss']
})
export class TripCreatePanelComponent implements OnInit {
    public tripForm: FormGroup;
    public submitted: boolean;
    public isNew: boolean;

    @Input()
    trip: Trip;

    @Output()
    onSubmit: EventEmitter<Trip> = new EventEmitter<Trip>();

    @Output()
    delete = new EventEmitter();

    @Output()
    onCancel = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder
    ) {
        this.tripForm = formBuilder.group({
            name: ['', [<any>Validators.required]],
            description: ['']
        });
    }

    public ngOnInit() {
        this.isNew = !!!this.trip;
        if (this.trip) {
            this.tripForm.controls['name'].setValue(this.trip.name);
            this.tripForm.controls['description'].setValue(this.trip.description);
        }
    }

    public onsubmit(value: Trip, valid: boolean) {
        this.submitted = true;
        if (valid) {
            this.onSubmit.emit(value);
        }
    }

    public ondelete() {
        this.delete.emit();
    }

    public oncancel() {
        this.onCancel.emit();
    }

    isControlInError(control) {
        return !control.valid && (control.touched || this.submitted);
    }
}
