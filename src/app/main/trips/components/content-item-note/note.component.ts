import { Component, Input } from '@angular/core';
import { NoteContent } from '../../../../models';

@Component({
    selector: 'app-content-note',
    templateUrl: 'note.component.html',
    styleUrls: ['./note.style.scss']
})
export class ContentItemNoteComponent {
    @Input() note: NoteContent;
    constructor() { }
}
