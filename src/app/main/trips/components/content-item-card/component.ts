import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog } from '@angular/material';
import { ContentDialogComponent } from '../content-item-dialog';
import { BaseContent, ContentType } from '../../../../models';

@Component({
    selector: 'app-content-card',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentItemCardComponent {
    public contentType = ContentType;
    public editMode = false;

    @Input() content: BaseContent;

    @Output() onEdit = new EventEmitter();

    @Output() onDelete = new EventEmitter();

    constructor(private dialog: MdDialog) { }

    edit() {
        let dialogRef = this.dialog.open(ContentDialogComponent);
        dialogRef.componentInstance.inputContent = this.content;
        this.editMode = false;
        dialogRef.afterClosed().subscribe(result => {
            this.onEdit.emit(result);
        });
    }

    delete() {
        if (confirm('Are you sure you want to delete this item?')) {
            this.onDelete.emit();
        }
    }
}
