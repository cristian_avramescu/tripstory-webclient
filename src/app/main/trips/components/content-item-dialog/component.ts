import { Component, Input, OnInit } from '@angular/core';
import { MdDialogRef, MdDialog } from '@angular/material';
import { BaseContent, ContentType } from '../../../../models';
import { ComponentItemLocationDialogComponent } from '../location-dialog';

@Component({
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentDialogComponent implements OnInit {
    public contentType = ContentType;

    public isDateTimePickerOpen = false;
    public creationDate = new Date();
    public dateTimePickerType = 'date';
    public isNew = false;

    public inputContent: BaseContent;
    public content: BaseContent;

    constructor(
        public dialogRef: MdDialogRef<ContentDialogComponent>,
        private dialog: MdDialog
    ) { }

    ngOnInit() {
        if (this.inputContent) {
            this.content = Object.assign({}, this.inputContent);
        }
    }

    onsubmit() {
        let result = null;
        if (this.inputContent && this.content) {
            result = Object.assign({}, this.inputContent, this.content);
        }
        this.dialogRef.close(result);
    }

    private openDateTimePicker() {
        this.isDateTimePickerOpen = true;
        setTimeout(() => {
            this.isDateTimePickerOpen = false;
        }, 1000);
    }

    openDatePicker() {
        this.dateTimePickerType = 'date';
        this.openDateTimePicker();
    }

    openTimePicker() {
        this.dateTimePickerType = 'time';
        this.openDateTimePicker();
    }

    openLocationDialog() {
        let dialogRef = this.dialog.open(ComponentItemLocationDialogComponent);
        dialogRef.componentInstance.location = this.content.location;
        dialogRef.afterClosed().subscribe(location => {
            if (location) {
                this.content.location = location;
            }
        });
    }
}
