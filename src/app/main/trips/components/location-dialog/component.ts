import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { ItemLocation } from '../../../../models';

@Component({
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ComponentItemLocationDialogComponent {
    public location: ItemLocation;

    constructor(public dialogRef: MdDialogRef<ComponentItemLocationDialogComponent>) { }

    onsubmit() {
        this.dialogRef.close(this.location);
    }

    mapClick(location: ItemLocation) {
        this.location = location;
    }
}
