import { Component, Input, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { BaseContent } from '../../../../models';
import { ComponentItemLocationDialogComponent } from '../location-dialog';

@Component({
    selector: 'app-date-location-picker',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class DateLocationPickerComponent implements OnInit {
    public isDateTimePickerOpen = false;
    public dateTimePickerType = 'date';

    @Input() public content: BaseContent;

    constructor(
        private dialog: MdDialog
    ) {
    }

    ngOnInit() {
        if (this.content) {
            this.content.creationDate = new Date(this.content.creationDate);
        }
    }

    private openDateTimePicker() {
        this.isDateTimePickerOpen = true;
        setTimeout(() => {
            this.isDateTimePickerOpen = false;
        }, 1000);
    }

    openDatePicker() {
        this.dateTimePickerType = 'date';
        this.openDateTimePicker();
    }

    openTimePicker() {
        this.dateTimePickerType = 'time';
        this.openDateTimePicker();
    }

    openLocationDialog() {
        let dialogRef = this.dialog.open(ComponentItemLocationDialogComponent);
        dialogRef.componentInstance.location = this.content.location;
        dialogRef.afterClosed().subscribe(location => {
            if (location) {
                this.content.location = location;
            }
        });
    }
}
