import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-item-content-edit-overlay',
    templateUrl: 'component.html',
    styleUrls: ['./style.scss']
})
export class ContentItemEditOverlayComponent {

    @Output() onEdit = new EventEmitter();

    @Output() onDelete = new EventEmitter();

    @Output() onCancel = new EventEmitter();

    constructor() { }

    onCancelHandler() {
        this.onCancel.emit();
    }

    onDeleteHandler() {
        this.onDelete.emit();
    }

    onEditHandler() {
        this.onEdit.emit();
    }
}
