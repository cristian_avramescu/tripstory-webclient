import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { GooglePlacesApi } from './google-places.service';
import { ItemLocation } from '../../../../models';

@Component({
    selector: 'app-google-map',
    styleUrls: ['./style.scss'],
    templateUrl: 'component.html'
})
export class GoogleMapComponent implements OnInit {
    private defaultLatitude = 52.370216;
    private defaultLongitude = 4.895168;
    public searchControl: FormControl;

    @ViewChild('map') public mapElementRef: ElementRef;
    @ViewChild('search') public searchElementRef: ElementRef;

    @Input('itemLocation') public itemLocation: ItemLocation;
    @Output('mapClick') public mapClick: EventEmitter<ItemLocation> = new EventEmitter<ItemLocation>();

    private mapDefaultProperties = {
        zoom: 12,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: false
    };

    private map: google.maps.Map;
    private marker: google.maps.Marker;
    private infoWindow: google.maps.InfoWindow;

    constructor(
        private googlePlacesApi: GooglePlacesApi
    ) { }

    ngOnInit() {
        this.initMap();
        this.initSearch();
        this.setInitialLocation();
        this.fixInfoWindow();
    }

    private initMap() {
        this.map = new google.maps.Map(this.mapElementRef.nativeElement, this.mapDefaultProperties);

        let timeout;
        this.map.addListener('click', ($event: google.maps.IconMouseEvent) => {
            timeout = setTimeout(() => {
                this.addPlaceDetails($event);
            }, 500);
        });
        this.map.addListener('dblclick', () => {
            clearTimeout(timeout);
        });
    }

    private initSearch() {
        this.searchControl = new FormControl();
        let searchBox = new google.maps.places.SearchBox(this.searchElementRef.nativeElement);
        searchBox.addListener('places_changed', () => {
            let places = searchBox.getPlaces();

            if (places.length === 0) {
                return;
            }

            let place = places[0];
            let location: ItemLocation = new ItemLocation(place.geometry.location.lat(), place.geometry.location.lng());
            location.placeId = place.place_id;
            location.placeName = place.name;
            location.address = place.formatted_address;
            location = this.googlePlacesApi.setAddressInfo(location, place.address_components);

            this.changeMapToItemLocation(location);
            this.emitLocationChange(location);
        });
    }

    private setInitialLocation() {
        if (this.itemLocation) {
            this.changeMapToItemLocation(this.itemLocation);
        } else if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                let location = new ItemLocation(position.coords.latitude, position.coords.longitude);
                this.changeMapToItemLocation(location);
                this.googlePlacesApi
                    .getDetailsFromLocation(this.map, location)
                    .then((l: ItemLocation) => {
                        this.emitLocationChange(l);
                    })
            }, () => {
                this.setDefaultInitialLocation();
            });
        } else {
            this.setDefaultInitialLocation();
        }
    }

    private setDefaultInitialLocation() {
        this.changeMapToItemLocation(new ItemLocation(this.defaultLatitude, this.defaultLongitude));
    }

    private changeMapToItemLocation(location: ItemLocation) {
        this.createMarker(location);
        if (this.infoWindow) {
            this.infoWindow.close();
        }
        this.map.setZoom(15);
        this.map.setCenter(new google.maps.LatLng(location.lat, location.lng));
    }

    private addPlaceDetails($event: google.maps.IconMouseEvent) {
        this.googlePlacesApi.getDetailsFromMouseEvent(this.map, $event).then((location: ItemLocation) => {
            this.createMarker(location);
            this.emitLocationChange(location);
        });
    }

    private cleanMarker() {
        if (this.infoWindow) {
            this.infoWindow.close();
        }
        if (this.marker) {
            this.marker.setMap(null);
        }
    }

    private createMarker(location: ItemLocation) {
        this.cleanMarker();
        let locationLatLng = new google.maps.LatLng(location.lat, location.lng);
        this.marker = new google.maps.Marker({
            position: locationLatLng,
            map: this.map
        });
        if (location.address) {
            google.maps.event.addListener(this.marker, 'click', () => {
                this.setInfoWindow(location.address, location.placeName);
            });
        }
    }

    private setInfoWindow(address: string, placeName?: string) {
        if (!this.infoWindow) {
            this.infoWindow = new google.maps.InfoWindow();
            this.infoWindow.set('noSuppress', true);
        }
        let content = '';
        if (placeName) {
            content += '<div><strong>' + placeName + '</strong><br>';
        }
        content += '<div>' + address + '</div>';
        this.infoWindow.setContent(content);
        this.infoWindow.open(this.map, this.marker);
    }

    private emitLocationChange(event: ItemLocation) {
        this.mapClick.emit(event);
    }

    private fixInfoWindow() {
        // Here we redefine the set() method.
        // If it is called for map option, we hide the InfoWindow, if 'noSuppress'
        // option is not true. As Google Maps does not know about this option,
        // its InfoWindows will not be opened.

        let set = google.maps.InfoWindow.prototype.set;
        google.maps.InfoWindow.prototype.set = function (key, val) {
            if (key === 'map' && !this.get('noSuppress')) {
                return;
            }

            set.apply(this, arguments);
        };
    }
}
