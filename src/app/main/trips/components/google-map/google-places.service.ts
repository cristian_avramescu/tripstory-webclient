import { Injectable } from '@angular/core';
import { ItemLocation } from '../../../../models';

@Injectable()
export class GooglePlacesApi {
    private placesService: google.maps.places.PlacesService;
    private geocoder: google.maps.Geocoder;

    public getDetailsFromMouseEvent(map: google.maps.Map, $event: google.maps.IconMouseEvent) {
        let location: ItemLocation = this.parseIconMouseEvent($event);
        return this.getDetailsFromLocation(map, location);
    }

    public getDetailsFromLocation(map: google.maps.Map, location: ItemLocation) {
        if (location.placeId) {
            return this.getPlaceDetails(map, location);
        } else {
            return this.getLocationAddress(location);
        }
    }

    public setAddressInfo(location: ItemLocation, addressComponents: google.maps.GeocoderAddressComponent[]) {
        addressComponents.map((component: google.maps.GeocoderAddressComponent) => {
            if (component.types[0] === 'locality' ||
                (component.types[0] === 'administrative_area_level_3' &&
                    !location.city)) {
                location.city = component.long_name;
            } else if (component.types[0] === 'country') {
                location.country = component.long_name;
            }
        });
        return location;
    }

    private getPlaceDetails(map: google.maps.Map, location: ItemLocation) {
        return new Promise((resolve, reject) => {
            this.getPlacesService(map).getDetails({ placeId: location.placeId }, (place, status) => {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    location.placeName = place.name;
                    location.address = place.formatted_address;
                    location = this.setAddressInfo(location, place.address_components);
                }
                resolve(location);
            });
        });
    }

    private getLocationAddress(location: ItemLocation) {
        return new Promise((resolve, reject) => {
            let locationLatLng = new google.maps.LatLng(location.lat, location.lng);
            this.getGeocoder().geocode({ 'location': locationLatLng }, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK && results[1]) {
                    location.address = results[1].formatted_address;
                    location.placeId = results[1].place_id;
                    location = this.setAddressInfo(location, results[1].address_components);
                }
                resolve(location);
            });
        });
    }

    private getPlacesService(map: google.maps.Map): google.maps.places.PlacesService {
        if (!this.placesService) {
            this.placesService = new google.maps.places.PlacesService(map);
        }
        return this.placesService;
    }

    private getGeocoder(): google.maps.Geocoder {
        if (!this.geocoder) {
            this.geocoder = new google.maps.Geocoder;
        }
        return this.geocoder;
    }

    private parseIconMouseEvent($event: google.maps.IconMouseEvent): ItemLocation {
        let result: ItemLocation = new ItemLocation($event.latLng.lat(), $event.latLng.lng());
        result.placeId = $event.placeId;
        return result;
    }


}
