import { Routes } from '@angular/router';
import {
    TripCreateComponent,
    TripInstanceComponent,
    TripListComponent,
    TripPropertiesComponent,
    TripContentComponent
} from './main-components';

export const TRIPS_ROUTES: Routes = [
    { path: '', component: TripListComponent },
    { path: 'trips/new', component: TripCreateComponent },
    {
        path: 'trips/edit/:id',
        component: TripInstanceComponent,
        children: [
            { path: '', component: TripContentComponent },
            { path: 'properties', component: TripPropertiesComponent }
        ]
    }
];
