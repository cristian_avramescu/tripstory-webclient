import { NgModule, } from '@angular/core';
import { CoreModule } from '../../core';
import { Md2Module } from 'md2';
import {
    TripCreateComponent,
    TripInstanceComponent,
    TripListComponent,
    TripPropertiesComponent,
    TripContentComponent
} from './main-components';
import {
    ComponentItemLocationDialogComponent,
    ContentItemEditOverlayComponent,
    ContentItemNoteComponent,
    ContentDialogComponent,
    GoogleMapComponent,
    GooglePlacesApi,
    TripCreatePanelComponent,
    TripPropertiesPanelComponent,
    DateLocationPickerComponent,
    ContentItemCardComponent,
    ContentItemNoteInputComponent,
    ContentItemImageUploadComponent,
    ContentItemImageComponent
} from './components';
import {
    ContentSortPipe,
    TextFormatPipe,
    LocationPipe
} from './pipes';
import {
    SmdFabSpeedDialActions,
    SmdFabSpeedDialComponent,
    SmdFabSpeedDialTrigger
} from './temp-components';

const pipes = [
    ContentSortPipe,
    TextFormatPipe,
    LocationPipe
];

const tempComponents = [
    SmdFabSpeedDialActions,
    SmdFabSpeedDialComponent,
    SmdFabSpeedDialTrigger
];

const mainComponents = [
    TripCreateComponent,
    TripInstanceComponent,
    TripListComponent,
    TripPropertiesComponent,
    TripContentComponent
];

const components = [
    ComponentItemLocationDialogComponent,
    ContentItemEditOverlayComponent,
    ContentItemNoteComponent,
    ContentDialogComponent,
    GoogleMapComponent,
    TripCreatePanelComponent,
    TripPropertiesPanelComponent,
    DateLocationPickerComponent,
    ContentItemCardComponent,
    ContentItemNoteInputComponent,
    ContentItemImageUploadComponent,
    ContentItemImageComponent
];

@NgModule({
    imports: [
        CoreModule,
        Md2Module.forRoot()
    ],
    exports: [
        CoreModule,
        ...mainComponents
    ],
    declarations: [
        ...mainComponents,
        ...components,
        ...pipes,
        ...tempComponents
    ],
    entryComponents: [
        ContentDialogComponent,
        ComponentItemLocationDialogComponent
    ],
    providers: [
        GooglePlacesApi
    ]
})
export class TripsModule { }
