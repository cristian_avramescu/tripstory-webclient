import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { TripsService } from '../../../../core';
import { Trip } from '../../../../models';
import { AutoUnsubscribe } from '../../../../decorators/autounsubscribe';

@Component({
  styleUrls: ['./instance.component.scss'],
  templateUrl: './instance.component.html'
})
export class TripInstanceComponent implements OnInit {
  public loading = true;
  public trip: Trip;
  private updating = false;
  public pending: boolean;

  get$: Subscription = null;
  getPending$: Subscription = null;

  constructor(
    private service: TripsService,
    private route: ActivatedRoute
  ) {
    this.getPending$ = this.service.getPending().subscribe((pending: boolean) => {
      this.pending = pending;
    });
  }

  ngOnInit() {
    let selectedId = this.route.params['value']['id'];
    this.get$ = this.service.get(selectedId).subscribe((trip: Trip) => {
      if (trip) {
        this.trip = trip;
        this.loading = false;
      }
    }).add(() => {
      this.service.removeSelected();
    });
  }
}
