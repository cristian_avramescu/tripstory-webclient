import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Trip } from '../../../../models';
import { TripsService } from '../../../../core';
import { AutoUnsubscribe } from '../../../../decorators/autounsubscribe';

@Component({
  styleUrls: ['./create.component.scss'],
  templateUrl: './create.component.html'
})
@AutoUnsubscribe
export class TripCreateComponent {
  add$: Subscription = null;

  constructor(
    private service: TripsService,
    private router: Router
  ) { }

  add(trip) {
    this.add$ = this.service.add(trip).subscribe(id => {
      if (id) {
        this.router.navigate(['trips/edit/', id]);
      }
    });
  }

  cancel() {
    this.router.navigate(['']);
  }
}
