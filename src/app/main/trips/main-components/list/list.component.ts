import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { TripsService } from '../../../../core';
import { Trip } from '../../../../models';
import { AutoUnsubscribe } from '../../../../decorators/autounsubscribe';

@Component({
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class TripListComponent implements OnInit {
    public list: Trip[];

    getAll$: Subscription = null;

    constructor(
        private service: TripsService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getAll$ = this.service.getAll().subscribe((list: Trip[]) => {
            this.list = list;
        });
    }

    create() {
        this.navigate(['/trips/new']);
    }

    open(trip) {
        this.navigate(['/trips/edit/', trip.id]);
    }

    private navigate(urlOptions: any[]) {
        this.router.navigate(urlOptions);
    }
}
