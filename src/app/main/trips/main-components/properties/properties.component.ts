import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { TripsService } from '../../../../core';
import { Trip } from '../../../../models';
import { AutoUnsubscribe } from '../../../../decorators/autounsubscribe';

@Component({
  styleUrls: ['./properties.component.scss'],
  templateUrl: './properties.component.html'
})
export class TripPropertiesComponent implements OnInit {
  public trip: Trip;
  private deleted = false;

  getSelected$: Subscription = null;

  constructor(
    private service: TripsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getSelected$ = this.service.getSelected().subscribe((trip: Trip) => {
      if (this.deleted && !this.service.isDefined(trip)) {
        this.goHome();
      }
      this.trip = trip;
    });
  }

  update(value: Trip) {
    let trip = Object.assign({}, this.trip, value);
    this.service.update(trip);
  }

  delete() {
    if (confirm('Are you sure you want to delete this trip?')) {
      this.deleted = true;
      this.service.delete(this.trip);
    }
  }

  goHome() {
    this.router.navigate(['']);
  }
}
