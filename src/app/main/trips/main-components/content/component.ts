import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { TripsService } from '../../../../core';
import { Trip, NoteContent, BaseContent, ImageContent, ContentType } from '../../../../models';
import { ContentSortPipe } from '../../pipes';
import { AutoUnsubscribe } from '../../../../decorators/autounsubscribe';
import { ContentDialogComponent } from '../../components';

@Component({
  styleUrls: ['./component.scss'],
  templateUrl: './component.html'
})
@AutoUnsubscribe
export class TripContentComponent implements OnInit {
  public contentType = ContentType;
  public trip: Trip = new Trip();

  getSelected$: Subscription = null;

  constructor(
    private service: TripsService,
    private dialog: MdDialog
  ) { }

  ngOnInit() {
    this.getSelected$ = this.service
      .getSelected()
      .subscribe((trip: Trip) => {
        if (trip) {
          this.trip = trip;
        }
      });
  }

  editItem(i: number, currentItem: BaseContent, newItem: BaseContent) {
    this.service.updateContentItem(this.trip, currentItem, newItem);
  }

  deleteItem(i: number, item: BaseContent) {
    this.service.deleteContentItem(this.trip, item);
  }

  private addContentItem(content: BaseContent) {
    let dialogRef = this.dialog.open(ContentDialogComponent);
    dialogRef.componentInstance.inputContent = content;
    dialogRef.componentInstance.isNew = true;
    dialogRef.afterClosed().subscribe((result: BaseContent) => {
      if (result) {
        this.service.addContentItem(this.trip, result);
      }
    });
  }

  addNote() {
    console.log(this.trip);
    this.addContentItem(new NoteContent());
  }

  addImage() {
    this.addContentItem(new ImageContent());
  }
}
