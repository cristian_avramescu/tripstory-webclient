import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { UsersService } from '../core';
import { User } from '../models';
import { ToolbarActions } from './components/toolbar';
import { AutoUnsubscribe } from '../decorators/autounsubscribe';


@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
@AutoUnsubscribe
export class MainComponent implements OnInit {
  public user: User;

  getUser$: Subscription = null;
  userLogout$: Subscription = null;

  constructor(
    private usersService: UsersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getUser$ = this.usersService.getUser().subscribe(user => {
      this.user = user;
    });
  }

  onToolbarAction(action: ToolbarActions) {
    switch (action) {
      case ToolbarActions.NavigateHome:
        this.router.navigate(['']);
        break;
      case ToolbarActions.Logout:
        this.logout();
        break;
    }
  }

  private logout() {
    this.userLogout$ = this.usersService.logout().subscribe(() => {
      this.router.navigate(['login']);
    });
  }
}
