import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { UserResolver, AuthGuard } from '../core';
import { TRIPS_ROUTES } from './trips';

const routes: Routes = [
    {
        path: '', component: MainComponent,
        resolve: { user: UserResolver },
        canActivate: [AuthGuard],
        children: [...TRIPS_ROUTES]
    }
];

export const MainRouting = RouterModule.forChild(routes);
