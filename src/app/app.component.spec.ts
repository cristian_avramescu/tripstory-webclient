import { TestBed, async } from '@angular/core/testing';
import { Router, RouterModule, RouterOutletMap } from '@angular/router';
import { AppComponent } from './app.component';
import { RouterMock } from './core/test/mocks';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: Router, useClass: RouterMock },
        RouterOutletMap
      ],
      imports: [
        RouterModule
      ]
    });
    TestBed.compileComponents();
  });

  it('should create the component', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
