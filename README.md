### Features ###

- tell a story
- add pictures, notes, automatic maps coordinates
- work on 'projects'
- combine text, pictures, videos to tell your story
- create a temporal path on a map to visualize the trip/vacation
- publish the story
- different levels of privacy for the final story (you want the public to see some stuff, but your friends or family a little bit more) ? don't really know how to achieve that (in term of design and usability); maybe we could go without for the beginning
- automatic timeline (each entry is tagged the day it is added)
- automatic geographic coordinates
- manual time tagging
- manual geographic tagging
- should have some templates ? (could start with just a word/blog type of arrangement)


### Structure ###

- project (this should be a trip, or a story you want to tell)
    - components:
        - picture
        - video
        - personal note (this would not be published in the final story, but you can always see it)
        - description (this would be a longer text where you would describe a part of the trip)
        - note (quick note to go along a picture/video/map entry)
        - map entry (pin point to a location => maybe you just want to add a note about a certain place)
    - organize the project
        - text editing
        - rearrange components order
    - publish the story
        - have a link to your story
        - possibility to share (FB, G+, IG, ?)


### Challenges ###

- templates or how to make it easy to present your story
- text editor


### Roadmap ###
- Project base
    - Client repository
    - Server repository
    - Auth - basics
    - App state management - basics
    - Configuration constants - basics
    - Error management - basics
    - Unit tests - basics
    - End to end tests - basics
    - Code coverage - basics
    - Build pipeline - basics
- Trips management
    - Create trip
    - Delete trip
    - Edit trip
    - Trip information
    - List trips
- Content creation
    - Add notes
    - 1 note = 1 day
    - Save/edit/delete existing notes
- Publish
    - Have a read only version of your text
    - Have a link you can give to your friends
- Deploy online
    - Online service to deploy the app to
        - Website
        - Server
        - Database
    - Automated deploy on push
- UI/UX
    - Logo
    - App theme
        - Design
        - Colors
    - UI library
    