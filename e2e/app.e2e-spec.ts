import { WebclientPage } from './app.po';

describe('webclient App', function() {
  let page: WebclientPage;

  beforeEach(() => {
    page = new WebclientPage();
  });

  it('should redirect to login page', () => {
    page.navigateTo();
    expect(page.getLocation()).toEqual('http://localhost:4200/login');
  });

  it('should login and redirect to home page', () => {
    page.navigateTo();
    page.fillInput('.login-form .email', 'test@test.com');
    page.fillInput('.login-form .password', 'pass');
    page.clickElement('loginSubmit');
    expect(page.getLocation()).toEqual('http://localhost:4200/');
    expect(page.getText('h2')).toEqual('Hello, test');
  });
});
