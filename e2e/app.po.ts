import { browser, element, by } from 'protractor';

export class WebclientPage {
  navigateTo(url?: string) {
    if(!url) {
      url = '/';
    }
    return browser.get(url);
  }

  getLocation() {
    return browser.getCurrentUrl();
  }

  getElementByCSS(cssSelector) {
    return element(by.css(cssSelector));
  }

  getElementById(id) {
    return element(by.id(id));
  }

  fillInput(selector, value: string) {
    this.getElementByCSS(selector).sendKeys(value);
  }

  clickElement(selector) {
    this.getElementById(selector).click();
  }

  getText(selector) {
    return this.getElementByCSS(selector).getText();
  }
}
